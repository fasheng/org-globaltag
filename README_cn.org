[[README.org][English]] | 简体中文

#+HTML: <a href="https://melpa.org/#/org-globaltag"><img alt="MELPA" src="https://melpa.org/packages/org-globaltag-badge.svg"/></a>
#+HTML: <a href="https://stable.melpa.org/#/org-globaltag"><img alt="MELPA Stable" src="https://stable.melpa.org/packages/org-globaltag-badge.svg"/></a>

*描述*: 包含分类信息的全局标签, 适用于Org mode. TODO

*关键字*: globaltag emacs org-mode tag category 标签 分类

*** 通过MELPA安装

#+begin_example
;; Install through package manager
M-x package-install <ENTER>
org-globaltag <ENTER>
#+end_example

*** 初始化
TODO

*** 屏幕截图
TODO

[[file:screenshot.png]]

*** 捐赠

- BTC [[https://www.blockchain.com/btc/address/3DMLQ8f4Ui5Adka9Y44MVM2cKT6yBVZdY5][3DMLQ8f4Ui5Adka9Y44MVM2cKT6yBVZdY5]]
- ETH [[https://www.blockchain.com/eth/address/0x561c694EF2bf32C23759c4Abd7D132161DaE13F8][0x561c694EF2bf32C23759c4Abd7D132161DaE13F8]]

*** 许可协议

GNU General Public License, Version 3.0+
