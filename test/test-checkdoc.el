(add-to-list 'load-path default-directory)

(defvar org-globaltag-sources (file-expand-wildcards "*.el"))

;; test byte-comple
;; (mapc #'byte-compile-file `(,cdnjs-el))

;; test checkdoc
(dolist (source org-globaltag-sources)
  (with-current-buffer (find-file-noselect source)
    (let ((checkdoc-diagnostic-buffer "*warn*"))
      (checkdoc-current-buffer t))))
