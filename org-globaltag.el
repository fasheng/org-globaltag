;;; org-globaltag.el --- Global tag contains the categories -*- lexical-binding: t; -*-

;; Author: Xu Fasheng <fasheng[AT]fasheng.info>
;; URL: https://gitea.com/fasheng/org-globaltag
;; Version: 0.0.1
;; Package-Version: 20201001.1234
;; Package-Requires : ((emacs "24.1") (org "8.2.6") (cl-lib "0.5"))

;;; Commentary:
;; TODO
;;
;; Usage:
;;
;;   ;; TODO

;;; Code:


(defgroup org-globaltag ()
  "TODO."
  :group 'comm)

(defcustom org-globaltag-tag "GLOBALTAG"
  "The tag on the trees containing the globaltag items."
  :group 'org-globaltag
  :type 'string)

(defvar org-globaltag-source-dir (if load-file-name
                                     (file-name-directory load-file-name)
                                   default-directory))

(defcustom org-globaltag-files (list (convert-standard-filename
                                      (expand-file-name "globaltag.org" org-globaltag-source-dir)))
  "The files where we look to find trees with the `org-globaltag-tag'."
  :group 'org-globaltag
  :type '(repeat (file :tag "org-mode file")))

(defcustom org-globaltag-user-files (list (concat user-emacs-directory "globaltag.org"))
  "User files to override `org-globaltag-files'."
  :group 'org-globaltag
  :type '(repeat (file :tag "org-mode file")))

;;;###autoload
(defun org-globaltag-enable ()
  "Enable globaltag function for Org mode."
  (interactive)
  ;TODO:
  )

;;;###autoload
(defun org-globaltag-disable ()
  "Disable globaltag function for Org mode."
  (interactive)
  ;TODO:
  )

(provide 'org-globaltag)

;;; org-globaltag.el ends here
